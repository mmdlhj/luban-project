package com.tarena.tp.luban.order.server.protocol.param;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderSignParam {
    String orderNo;
}
