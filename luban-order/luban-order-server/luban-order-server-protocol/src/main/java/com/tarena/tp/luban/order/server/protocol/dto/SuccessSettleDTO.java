/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.order.server.protocol.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class SuccessSettleDTO implements Serializable {


    private static final long serialVersionUID = 1201210848432131697L;

    /**
     *  金额
     */
    private Long amount;


    /**
     *  渠道: 1-pc 2-app
     */
    private Integer channel;

    private String createTime;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 支付时间
     */
    private String payTime;

    /**
     * 支付平台 1-微信 2-支付宝
     */
    private Integer platform;

    /**
     * 1-订单中心
     */
    private Integer sourceType;

    private Integer status;

    /**
     * 第三方交易流水号
     */
    private String thirdOrderNo;

    private String title;

    private String updateTime;

    private Long userId;



}
