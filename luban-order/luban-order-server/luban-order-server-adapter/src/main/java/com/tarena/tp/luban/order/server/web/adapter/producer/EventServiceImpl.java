package com.tarena.tp.luban.order.server.web.adapter.producer;

import com.tarena.tp.luban.order.server.domain.service.EventService;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class EventServiceImpl implements EventService {
    @Autowired
    private RocketMQTemplate rocketTemplate;
    //师傅点击完成，异步发送消息,记录账单 模拟打款
    public static final String ORDER_FINISH_TOPIC="order_finish_topic";
    @Override
    public void sendOrderFinishEvent(OrderMqDTO orderMq) {
        //1. 组织一个消息对象 message
        Message<OrderMqDTO> message = MessageBuilder.withPayload(orderMq).build();
        //2. 异步发送
        rocketTemplate.asyncSend(ORDER_FINISH_TOPIC, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("发送成功");
            }
            @Override
            public void onException(Throwable throwable) {
                System.out.println("发送异常");
            }
        });
    }

    @Override
    public void sendOrderCancelEvent(OrderMqDTO orderMqDTO) {

    }

    @Override
    public void sendDelaySignEvent(String orderNo) {
        //封装消息对象
        Message<String> message = MessageBuilder.withPayload(orderNo).build();
        //发送延迟消息 order_delay_topic 延迟30秒
        rocketTemplate.syncSend("order_delay_topic",message,1000,4);
    }
}
