package com.tarena.tp.luban.order.server.web.adapter.controller;

import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.service.OrderService;
import com.tarena.tp.luban.order.server.protocol.param.OrderConfirmParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderFinishParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.param.OrderSignParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tarena.tp.luban.order.server.web.adapter.assemble.OrderAssemble;
import com.tarena.tp.luban.order.server.web.adapter.vo.OrderDetailVO;
import com.tarena.tp.luban.order.server.web.adapter.vo.OrderVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.model.Result;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;
    /**
     * 抢单,生成订单
     */
    @PostMapping("/order/create")
    public Result<String> create(@RequestBody OrderParam orderParam) throws BusinessException{
        //调用业务层
        String orderNo=orderService.create(orderParam);
        return new Result<>("");
    }
    /**
     * 师傅工作太订单列表
     */
    @Autowired
    private OrderAssemble orderAssemble;
    @PostMapping("/order/list")
    public Result<PagerResult<OrderVO>> orderList(@RequestBody OrderQuery orderQuery)
            throws BusinessException
    {
        //1. 查询一个带有total和list分页数据的对象
        ListRecordTotalBO<OrderBO> orderBOS=orderService.orderList(orderQuery);
        //2. 转化
        return new Result<>(orderAssemble.assemblePagerResult(orderBOS,orderQuery));
    }
    /**
     * 订单详情查询
     */
    @GetMapping("/order/info")
    public Result<OrderDetailVO> orderDetail(String orderNo) throws BusinessException {
        //业务返回BO
        OrderBO orderBO=orderService.orderDetail(orderNo);
        //装配器转化VO
        return new Result<>(orderAssemble.assembleBO2DetailVO(orderBO));
    }
    /**
     * 师傅签到
     */
    @PostMapping("/order/sign")
    public Result<String> sign(@RequestBody OrderSignParam orderSignParam)
            throws BusinessException{
        return new Result(orderService.sign(orderSignParam));
    }
    /**
     * 施工图片 绑定 | 开始施工状态
     */
    @PostMapping("/order/confirm")
    public Result<String> confirm(@RequestBody OrderConfirmParam orderConfirmParam)
            throws BusinessException{
        return new Result(orderService.confirm(orderConfirmParam));
    }
    /**
     * 师傅点击完成订单
     * 记录状态完成待结算 finish
     * complete(彻底完成)
     */
    @PostMapping("/order/finish")
    public Result<String> finish(@RequestBody OrderFinishParam orderFinishParam)
            throws BusinessException{
        return new Result(orderService.finish(orderFinishParam));
    }

}






























