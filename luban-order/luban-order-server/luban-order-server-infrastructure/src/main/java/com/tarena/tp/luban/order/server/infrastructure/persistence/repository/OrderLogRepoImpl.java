package com.tarena.tp.luban.order.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.order.po.OrderLog;
import com.tarena.tp.luban.order.server.dao.OrderLogDAO;
import com.tarena.tp.luban.order.server.domain.bo.OrderLogBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderLogRepository;
import com.tarena.tp.luban.order.server.infrastructure.persistence.data.converter.OrderConverter;
import com.tarena.tp.luban.order.server.infrastructure.persistence.data.converter.OrderLogConverter;
import com.tarena.tp.luban.order.server.protocol.param.OrderLogParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderLogRepoImpl implements OrderLogRepository {
    @Autowired
    private OrderLogDAO orderLogDAO;
    @Autowired
    private OrderLogConverter orderLogConverter;
    @Override
    public Long save(OrderLogParam orderLogParam) {
        OrderLog orderLog = orderLogConverter.param2po(orderLogParam);
        return orderLogDAO.insert(orderLog);
    }

    @Override
    public Integer delete(Long orderLogId) {
        return null;
    }

    @Override
    public List<OrderLogBO> getOrderLogByOrderNo(String orderLogId) {
        return null;
    }

    @Override
    public List<OrderLogBO> selectFinishOrderLog(String orderNo) {
        return null;
    }
}
