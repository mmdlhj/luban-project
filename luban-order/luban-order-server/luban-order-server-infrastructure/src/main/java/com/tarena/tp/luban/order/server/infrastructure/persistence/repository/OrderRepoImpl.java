package com.tarena.tp.luban.order.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.order.po.Order;
import com.tarena.tp.luban.order.server.dao.OrderDAO;
import com.tarena.tp.luban.order.server.dao.query.OrderDBPagerQuery;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderRepository;
import com.tarena.tp.luban.order.server.infrastructure.persistence.data.converter.OrderConverter;
import com.tarena.tp.luban.order.server.protocol.param.OrderParam;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderRepoImpl implements OrderRepository {
    @Autowired
    private OrderDAO orderDAO;
    @Autowired
    private OrderConverter orderConverter;
    @Override
    public Long save(OrderParam orderParam) {
        Order order = orderConverter.param2po(orderParam);
        return orderDAO.insert(order);
    }

    @Override
    public Long getOrderCount(OrderQuery orderQuery) {
        //转化dbquery
        OrderDBPagerQuery dbPagerQuery = orderConverter.toDbPagerQuery(orderQuery);
        //查询count
        return orderDAO.countOrder(dbPagerQuery);
    }
    @Override
    public List<OrderBO> queryOrders(OrderQuery orderQuery) {
        //转化dbQuery
        OrderDBPagerQuery dbPagerQuery = orderConverter.toDbPagerQuery(orderQuery);
        //查询dolist
        List<Order> orders = orderDAO.queryOrders(dbPagerQuery);
        //转化bolist
        return orderConverter.poList2BoList(orders);
    }
    @Override
    public Integer delete(Long orderId) {
        return null;
    }

    @Override
    public OrderBO getOrder(Long orderId) {
        return null;
    }

    @Override
    public OrderBO getOrderByOrderNo(String orderNo) {
        //查询 do
        Order order = orderDAO.getOrderByOrderNo(orderNo);
        //转化 返回
        return orderConverter.po2bo(order);
    }



    @Override
    public Integer disable(String bankIds) {
        return null;
    }

    @Override
    public Integer enable(String bankIds) {
        return null;
    }

    @Override
    public void sign(OrderParam orderParam) {
        //转化
        Order order = orderConverter.param2po(orderParam);
        //dao调用
        orderDAO.updateStatus(order);
    }

    @Override
    public void confirm(OrderParam orderParam) {
        //转化
        Order order = orderConverter.param2po(orderParam);
        //dao调用
        orderDAO.updateStatus(order);
    }

    @Override
    public int finish(OrderParam orderParam) {
        //转化
        Order order = orderConverter.param2po(orderParam);
        //dao调用
        return orderDAO.updateStatus(order);
    }

    @Override
    public void complete(OrderParam orderParam) {
        //转化
        Order order = orderConverter.param2po(orderParam);
        //dao调用
        orderDAO.updateStatus(order);
    }
}
