package com.tarena.tp.luban.order.server.domain.service;

import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.param.AttachParam;
import com.tarena.tp.attach.server.param.AttachUpdateParam;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.demand.server.client.RequestOrderApi;
import com.tarena.tp.luban.order.common.enums.OrderStatusEnum;
import com.tarena.tp.luban.order.common.enums.ResultEnum;
import com.tarena.tp.luban.order.server.domain.assembler.OrderAssembler;
import com.tarena.tp.luban.order.server.domain.bo.AttachInfoBO;
import com.tarena.tp.luban.order.server.domain.bo.OrderBO;
import com.tarena.tp.luban.order.server.domain.repository.OrderLogRepository;
import com.tarena.tp.luban.order.server.domain.repository.OrderRepository;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.order.server.protocol.param.*;
import com.tarena.tp.luban.order.server.protocol.query.OrderQuery;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.model.Result;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class OrderService {
    @Autowired
    private RequestOrderApi requestOrderApi;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderLogRepository orderLogRepository;
    @Autowired
    private OrderAssembler orderAssembler;
    public String create(OrderParam orderParam) throws BusinessException {
        //TODO 查询师傅当前未完成订单个数 select count(*) from order where status in (10,20,50)
        //1. RPC远程调用 拿到抢单结果.抢到了创建订单,没抢到 返回抢单失败 TODO
        Boolean grabOrder = requestOrderApi.grabOrder(orderParam.getRequestOrderNo());
        if (!grabOrder){
            throw new BusinessException(ResultEnum.ORDER_GRAB_FAIL);
        }
        //2. 生成订单 绑定的当前登录的师傅userId. saveOrder
        //orderParam 缺少数据 userId orderNo OrderStatus 10 抢单成功
        //20 签到 50 施工中 30 完成未结算 40 完成 -10 -20 -30
        Long userId = SecurityContext.getLoginToken().getUserId();
        String orderNo= UUID.randomUUID().toString();
        Integer status= OrderStatusEnum.ORDER_CREATE_SUCCESS.getStatus();//10
        orderParam.setUserId(userId);
        orderParam.setOrderNo(orderNo);
        orderParam.setStatus(status);
        orderRepository.save(orderParam);
        //3. 当前订单的日志状态,抢单成功. saveOrderLog
        //拿到新增的orderLogParam对象
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog
                (orderNo, userId, OrderStatusEnum.ORDER_CREATE_SUCCESS);
        orderLogRepository.save(orderLogParam);
        //添加代码,将这个订单编号作为消息 发送延迟消息
        eventService.sendDelaySignEvent(orderNo);
        return orderNo;
    }

    public ListRecordTotalBO<OrderBO> orderList(OrderQuery orderQuery) {
        //查询的是当前登录的师傅的订单数据,属性缺少userId必要条件.
        Long userId = SecurityContext.getLoginToken().getUserId();
        orderQuery.setUserId(userId);
        //1. 查询total
        Long total = orderRepository.getOrderCount(orderQuery);
        List<OrderBO> orderBOS=null;
        //2. 查询list
        if (total>0){
            orderBOS=orderRepository.queryOrders(orderQuery);
        }
        ListRecordTotalBO<OrderBO> orders=new ListRecordTotalBO<>(orderBOS,total);
        return orders;
    }
    @Autowired
    private AttachApi attachApi;
    public OrderBO orderDetail(String orderNo) throws BusinessException {
        //1. 使用orderNo查询订单 bo对象
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //2. 远程调用attachApi 传递businessId=orderId businessType=200
        if (orderBO == null || orderBO.getId()==null){
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        AttachQuery attachQuery=new AttachQuery();
        attachQuery.setBusinessId(orderBO.getId().intValue());
        attachQuery.setBusinessType(200);
        //需要检查dubbo配置
        List<AttachDTO> attachDTOS = attachApi.getAttachInfoByParam(attachQuery);
        //将attachDTOS转化成attachInfoBOS
        if (CollectionUtils.isEmpty(attachDTOS)){
            orderBO.setAttachInfoBO(null);
        }else {
            List<AttachInfoBO> attaches=new ArrayList<>();
            for (AttachDTO attachDTO : attachDTOS) {
                String url="http://localhost:8092/static/"+attachDTO.getFileUuid();
                AttachInfoBO attachInfoBO=new AttachInfoBO();
                attachInfoBO.setUrl(url);
                attaches.add(attachInfoBO);
            }
            orderBO.setAttachInfoBO(attaches);
        }
        return orderBO;
    }

    public String sign(OrderSignParam orderSignParam) throws BusinessException {
        //只有订单抢单成功的状态才能转成签到 验证一下流转状态
        //1. orderNo查询 订单 当前状态
        String orderNo=orderSignParam.getOrderNo();
        //2. 非空情况下,判断流转是否正常
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        if (orderBO==null|| orderBO.getId()==null){
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //如果订单当前状态不是抢单成功 10 流转错误
        if (orderBO.getStatus()!=10){
            throw new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR);
        }
        //3. 如果一切正常 在执行update更新 和 日志表格数据记录
        OrderParam orderParam=new OrderParam();//update order set status=20 where order_no=#{}
        orderParam.setStatus(20);
        orderParam.setOrderNo(orderNo);
        orderRepository.sign(orderParam);
        //日志记录
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(
                orderNo,
                SecurityContext.getLoginToken().getUserId(),
                OrderStatusEnum.ORDER_SIGN_SUCCESS);
        orderLogRepository.save(orderLogParam);
        return orderNo;
    }
    public Object confirm(OrderConfirmParam orderConfirmParam) throws BusinessException {
        //1. 使用orderNo查询订单
        String orderNo=orderConfirmParam.getOrderNo();
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //2. 判断存在
        if (orderBO==null|| orderBO.getId()==null){
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //3. 判断状态流转是否异常 20签到转成50正在施工|图片上传 50 后续施工图片上传
        //如果订单当前状态不是抢单成功 10 流转错误
        if (orderBO.getStatus()!=20 && orderBO.getStatus()!=50){
            throw new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR);
        }
        //5.绑定图片 应该提前做绑定
        List<AttachUpdateParam> boundAttaches=new ArrayList<>();
        List<Long> attachIds = orderConfirmParam.getAttachIds();
        if (!CollectionUtils.isEmpty(attachIds)){
            //只有图片上传的id是非空的情况下,才进行绑定调用
            for (Long attachId : attachIds) {
                AttachUpdateParam attachUpdateParam=new AttachUpdateParam();
                attachUpdateParam.setIsCover(0);
                attachUpdateParam.setBusinessType(200);
                attachUpdateParam.setBusinessId(orderBO.getId().intValue());
                attachUpdateParam.setId(attachId.intValue());
                boundAttaches.add(attachUpdateParam);
            }
            attachApi.batchUpdateAttachByIdList(boundAttaches);
        }else{
            //上传的图片是空的,绑定异常
            throw new BusinessException(ResultEnum.ATTACH_CONFIRM_ERROR);
        }
        //4. 更改订单状态 记录日志信息(图片上传每一批,都记录一次状态数据)
        if (orderBO.getStatus()!=50){
            OrderParam orderParam=new OrderParam();
            orderParam.setOrderNo(orderNo);
            orderParam.setStatus(50);
            orderRepository.confirm(orderParam);
        }
        //日志记录
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(
                orderNo,
                SecurityContext.getLoginToken().getUserId(),
                OrderStatusEnum.ORDER_ATTACH_CONFIRM);
        orderLogRepository.save(orderLogParam);
        return orderNo;
    }
    @Autowired
    private EventService eventService;
    public Object finish(OrderFinishParam orderFinishParam) throws BusinessException {
        //1.查询订单
        String orderNo=orderFinishParam.getOrderNo();
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //2.判断存在和流转状态是否正常
        if (orderBO==null|| orderBO.getId()==null){
            throw new BusinessException(ResultEnum.ORDER_EMPTY);
        }
        //只有经过施工状态,才能流转到完成待结算
        if (orderBO.getStatus()!=50){
            throw new BusinessException(ResultEnum.ORDER_STATUS_TRANSFORM_ERROR);
        }
        //3.更新订单状态50 变成30
        OrderParam orderParam=new OrderParam();
        orderParam.setStatus(30);
        orderParam.setOrderNo(orderNo);
        orderRepository.finish(orderParam);
        //4.记录订单日志
        OrderLogParam orderLogParam = orderAssembler.generateOrderLog(
                orderNo,
                SecurityContext.getLoginToken().getUserId(),
                OrderStatusEnum.ORDER_SERVICE_SUCCESS_UN_SETTLE
        );
        orderLogRepository.save(orderLogParam);
        //5. 发送消息,包含当前订单的可以计算结算的所数据.
        OrderMqDTO orderMqDTO=orderAssembler.assembleOrderMqDTO(orderBO);
        eventService.sendOrderFinishEvent(orderMqDTO);
        return orderNo;
    }

    public void complete(String orderNo) {
        //1 查询订单
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //2 判断流转状态
        if (orderBO==null || orderBO.getId()==null){
            //记录日志
            log.info("当前订单没有数据,orderNo:{}",orderNo);
            return;
        }
        //完成的订单一定是30 变成40
        if (orderBO.getStatus()!=30){
            //记录日志
            log.info("订单流转状态不正确,orderNo:{},startus:{}",orderNo,orderBO.getStatus());
            return;
        }
        //3 更新订单状态为40 完成
        OrderParam orderParam=new OrderParam();
        orderParam.setOrderNo(orderNo);
        orderParam.setStatus(40);
        orderRepository.complete(orderParam);
        //4 记录订单日志 userId=999
        OrderLogParam orderLogParam=
        orderAssembler.generateOrderLog(orderNo,999L,OrderStatusEnum.ORDER_FINISH);
        orderLogRepository.save(orderLogParam);
    }

    public void delay(String orderNo) {
        //1. 查询订单
        OrderBO orderBO = orderRepository.getOrderByOrderNo(orderNo);
        //2. 判断存在前提下 是否status=10
        if (orderBO==null || orderBO.getId()==null){
            log.info("当前消费出现错误订单数据:{}",orderNo);
            return;
        }
        if (orderBO.getStatus()!=10){
            log.info("这是一张已经在处理操作的订单:{}",orderNo);
            return;
        }
        //3. 状态不正常 关闭订单-30,记录订单操作日志 平台关闭
        OrderParam orderParam=new OrderParam();
        orderParam.setStatus(-30);
        orderParam.setOrderNo(orderNo);
        orderRepository.finish(orderParam);
        OrderLogParam orderLogParam=
        orderAssembler.generateOrderLog(orderNo,999L,OrderStatusEnum.CANCELLED_BY_EXPIRE);
        orderLogRepository.save(orderLogParam);
        //4. 远程调用 return需求单 TODO demand服务没有实现归还
        requestOrderApi.returnOrder(orderBO.getRequestOrderNo());
    }
}




































