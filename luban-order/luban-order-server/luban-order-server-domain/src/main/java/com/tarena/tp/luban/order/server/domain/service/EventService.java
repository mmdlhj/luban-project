/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tarena.tp.luban.order.server.domain.service;

import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;

public interface EventService {
    /* 发送订单完成待结算的事件
     */
    void sendOrderFinishEvent(OrderMqDTO orderMq);
    /* 发送订单取消事件
     */
    void sendOrderCancelEvent(OrderMqDTO orderMqDTO);
    /* 发送延迟签到的事件
     */
    void sendDelaySignEvent(String orderNo);
}
