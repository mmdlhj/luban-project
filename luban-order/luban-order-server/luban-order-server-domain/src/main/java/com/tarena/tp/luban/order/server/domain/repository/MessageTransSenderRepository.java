package com.tarena.tp.luban.order.server.domain.repository;

import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;

public interface MessageTransSenderRepository {
    Boolean sendOrderTransMessage(OrderMqDTO orderMqDTO);
}
