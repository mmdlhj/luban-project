/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.admin.manager;

import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tarena.tp.luban.order.admin.bo.OrderBO;
import com.tarena.tp.luban.order.admin.repository.OrderRepository;
import com.tarena.tp.luban.order.admin.protocol.query.OrderQuery;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;


    public ListRecordTotalBO<OrderBO> queryOrder(OrderQuery orderQuery) {
        Long totalRecord = this.orderRepository.getOrderCount(orderQuery);
        List<OrderBO> orderBoList = new ArrayList<>();
        if (totalRecord > 0) {
            orderBoList = this.orderRepository.queryOrders(orderQuery);
        }
        return new ListRecordTotalBO<>(orderBoList, totalRecord);
    }

    public OrderBO getOrder(Long orderId)  {
        return this.orderRepository.getOrder(orderId);
    }
}