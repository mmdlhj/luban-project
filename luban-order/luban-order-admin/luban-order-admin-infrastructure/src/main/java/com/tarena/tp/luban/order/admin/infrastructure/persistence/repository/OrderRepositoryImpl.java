/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.admin.infrastructure.persistence.repository;

import com.tarena.tp.luban.order.admin.dao.OrderDAO;
import com.tedu.inn.protocol.enums.StatusRecord;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.order.admin.infrastructure.persistence.data.converter.OrderConverter;
import com.tarena.tp.luban.order.po.Order;
import com.tarena.tp.luban.order.admin.bo.OrderBO;
import com.tarena.tp.luban.order.admin.protocol.param.OrderParam;
import com.tarena.tp.luban.order.admin.repository.OrderRepository;
import com.tarena.tp.luban.order.admin.protocol.query.OrderQuery;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderRepositoryImpl implements OrderRepository {

    @Autowired
    private OrderConverter orderConverter;

    @Autowired
    private OrderDAO orderDao;

    @Override public OrderBO getOrder(Long orderId) {
        Order order = this.orderDao.getEntity(orderId);
        return this.orderConverter.po2bo(order);
    }

    @Override public List<OrderBO> queryOrders(
        OrderQuery orderQuery) {
        List<Order> orderList = this.orderDao.queryOrders(this.orderConverter.toDbPagerQuery(orderQuery));
        return this.orderConverter.poList2BoList(orderList);
    }

    @Override public Long getOrderCount(OrderQuery orderQuery) {
        return this.orderDao.countOrder(this.orderConverter.toDbPagerQuery(orderQuery));
    }
}