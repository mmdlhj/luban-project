/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.order.admin.controller;

import com.tarena.tp.luban.order.admin.assemble.OrderLogAssemble;
import com.tarena.tp.luban.order.admin.bo.OrderLogBO;
import com.tarena.tp.luban.order.admin.manager.OrderLogService;
import com.tarena.tp.luban.order.admin.vo.OrderLogVO;
import com.tedu.inn.protocol.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("admin/order/log")
@Api(value = "order",tags = "订单操作记录接口")
public class OrderLogController {

    @Resource
    private OrderLogService orderLogService;

    @Resource
    private OrderLogAssemble orderLogAssemble;


    @GetMapping(value = "get")
    @ApiOperation("获取订单操作记录")
    @ApiImplicitParam(name = "orderNo", value = "订单编号", paramType = "query", required = true)
    public List<OrderLogVO> getOrderLog(@RequestParam("orderNo") String orderNo) throws BusinessException {
        List<OrderLogBO> orderLogBo = orderLogService.getOrderLogByOrderNo(orderNo);
        return this.orderLogAssemble.boListAssembleVOList(orderLogBo);
    }


}