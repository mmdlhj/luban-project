/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.attach.server.param;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

@Data
public class AttachParam implements Serializable {

    @ApiModelProperty("系统文件名")
    private String fileUuid;

    @ApiModelProperty("文件名")
    private String clientFileName;

    @ApiModelProperty("文件类型")
    private Integer contentType;

    @ApiModelProperty("是否封面")
    private Integer isCover;

    @ApiModelProperty("系统类型")
    private Integer businessType;

    @ApiModelProperty("业务id")
    private Integer businessId;

    @ApiModelProperty("备注")
    private String remark;

    private Long contentLength;

    private Integer width;

    private Integer height;

}
