/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.attach.admin.web.assemble;

import com.tarena.tp.attach.admin.dto.AttachDTO;
import com.tarena.tp.attach.admin.web.vo.AttachVO;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AttachAssemble {

    public AttachVO assembleDtoToVo(AttachDTO attachDTO) {
        if (attachDTO == null) {
            return null;
        }
        AttachVO attachVO = new AttachVO();
        BeanUtils.copyProperties(attachDTO,attachVO);
        return attachVO;
    }

    public List<AttachVO> assembleDtoListToVoList(List<AttachDTO> sources) {
        if (sources == null) {
            return null;
        }
        List<AttachVO> objects = new ArrayList<>();
        for (AttachDTO source : sources) {
            AttachVO object = new AttachVO();
            BeanUtils.copyProperties(source,object);
            objects.add(object);
        }
        return objects;
    }
}
