package com.tarena.test.gateway.whitelist.test;

import com.tarena.tp.luban.gateway.filter.AuthenticationFilter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.util.AntPathMatcher;
@Slf4j
public class WhiteListTest {
    private static Map<String, List<String>> whiteLists=new HashedMap();
    static {
        List<String> uris=new ArrayList<>();
        uris.add("/login/**");
        uris.add("/register/*");
        whiteLists.put("www.passport.com",uris);
    }
    public static void main(String[] args) throws URISyntaxException {
        AntPathMatcher pathMatcher=new AntPathMatcher();
        String hostName="www.passport.com";
        URI uri=new URI("/login/kaka/haha");
        if (whiteLists == null || whiteLists.size()==0){
            log.debug("this application does not contain any white uris");
            //没有白名单,所有请求均需要认证
        }else {
            boolean containsHostName = whiteLists.containsKey(hostName);
            if (containsHostName){
                List<String> whiteUris = whiteLists.get(hostName);
                List<String> collect = whiteUris.stream().filter(matched -> {
                    return AuthenticationFilter.PATH_MATCHER.match(matched, uri.getPath());
                }).collect(Collectors.toList());
                //如果有匹配,返回结果不为空
                if (collect.size()!=0){
                    log.info("当前路径匹配到了白名单");
                }else{
                    log.info("当前路径乜有匹配白名单");
                }
            }
        }
    }
}
