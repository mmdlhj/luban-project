package com.tarena.tp.luban.settle.server.service;

import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.settle.server.assemble.SettleAssembler;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.server.repository.SettleBillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SettleService {
    @Autowired
    private SettleBillRepository settleBillRepository;
    //结算业务 在消费逻辑调用的,有可能重复消费的
    //同一个订单,是否允许计算生成2次账单信息
    @Autowired
    private SettleAssembler settleAssembler;
    @Autowired
    private AccountApi accountApi;
    @Autowired
    private EventService eventService;
    //添加本地事务注解,回退同一个数据源连接的数据库操作数据
    @Transactional
    public void settle(OrderMqDTO message) {
        //防止重复消费,无论消息被消费多少次,同一个消息,代表同一个订单,最多只能生成1对账单 平台一个,师傅一个
        //利用当前订单编号,先查询账单数据,如果为0 说明当前订单没有生成过账单,如果不为0,已经生成 方法不调用 TODO
        Long size = settleBillRepository.getSettleBillByOrderNo(message.getOrderNo());
        if (size>0){
            //账单已经记录过了,打款结束了.
            return;
        }
        //1. 计算账单 order_amount* profitScale分润 平台账单, order_amount*(1-profiteScale) 师傅账单
        //账单数据批量写入数据库一次写2条
        List<SettleBillParam> settleBillParams = settleAssembler.order2Params(message);
        settleBillRepository.batchSave(settleBillParams);
        //2. 远程调用account-server 模拟打款
        PaymentParam paymentParam=new PaymentParam();
        paymentParam.setOrderNo(message.getOrderNo());
        paymentParam.setUserId(message.getWorkerId());
        paymentParam.setTotalAmount(settleBillParams.get(0).getTotalAmount());
        accountApi.mockPayment(paymentParam);
        //3. 与order-server通信,将这张订单状态最终修改为完成 (发送消息) 消息精简和准确的设计
        eventService.sendSettledEvent(message.getOrderNo());
    }
}
