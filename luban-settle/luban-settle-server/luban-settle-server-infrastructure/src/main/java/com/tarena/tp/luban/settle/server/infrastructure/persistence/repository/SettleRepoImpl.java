package com.tarena.tp.luban.settle.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.settle.po.SettleBill;
import com.tarena.tp.luban.settle.server.dao.SettleBillDAO;
import com.tarena.tp.luban.settle.server.infrastructure.persistence.converter.SettleBillConverter;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.server.repository.SettleBillRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Repository
public class SettleRepoImpl implements SettleBillRepository {
    @Autowired
    private SettleBillDAO settleBillDAO;
    @Autowired
    private SettleBillConverter settleBillConverter;
    @Override
    public void batchSave(List<SettleBillParam> settleBillParams) {
        //转化 param2Po
        List<SettleBill> settleBills = settleBillConverter.params2pos(settleBillParams);
        //批量insert
        settleBillDAO.batchInsert(settleBills);
    }

    @Override
    public void updateStatus(String orderNo, Integer status) {

    }
    //select count() from settle_bill where order_no=#{orderNo}
    @Override
    public Long getSettleBillByOrderNo(String orderNo) {
        List<SettleBill> settleBills = settleBillDAO.getSettleBillByOrderNo(orderNo);
        //不一定能够查询到数据
        if (CollectionUtils.isEmpty(settleBills)){
            return 0L;
        }
        return settleBills.size()+0L;
    }
}
