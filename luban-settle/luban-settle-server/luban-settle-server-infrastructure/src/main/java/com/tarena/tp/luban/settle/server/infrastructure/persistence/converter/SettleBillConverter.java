/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.server.infrastructure.persistence.converter;

import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.settle.po.SettleBill;
import com.tarena.tp.luban.settle.server.bo.SettleBillBO;
import com.tarena.tp.luban.settle.server.protocol.param.SettleBillParam;
import com.tedu.inn.protocol.dao.StatusCriteria;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class SettleBillConverter {

     public SettleBill param2po(SettleBillParam param) {
        SettleBill settleBill = new SettleBill();
        BeanUtils.copyProperties(param, settleBill);
        settleBill.setRequestOrderPrice(param.getRequestOrderRawPrice());
        //todo 用户信息
        LoginUser loginUser = SecurityContext.getLoginToken();

        settleBill.setGmtCreate(System.currentTimeMillis());
        settleBill.setGmtModified(System.currentTimeMillis());
        settleBill.setCreateUserId(99L);
        settleBill.setModifiedUserId(99L);
        settleBill.setCreateUserName("mock");
        settleBill.setModifiedUserName("mock");
        return settleBill;
    }

    public SettleBill param2UpdatePO(String orderNo,Integer status) {
        SettleBill settleBill = new SettleBill();
        //todo 用户信息
        LoginUser loginUser = SecurityContext.getLoginToken();
        settleBill.setOrderNo(orderNo);
        settleBill.setStatus(status);
        settleBill.setPaymentTime(System.currentTimeMillis());
        settleBill.setGmtModified(System.currentTimeMillis());
        settleBill.setModifiedUserId(99L);
        settleBill.setModifiedUserName("mock");
        return settleBill;
    }

   public List<SettleBill> params2pos(List<SettleBillParam> settleBillParams) {
        if (CollectionUtils.isEmpty(settleBillParams)){
           return Collections.emptyList();
        }
        List<SettleBill> list = new ArrayList<>();
        settleBillParams.forEach(settleBillParam -> {
            SettleBill settleBill = param2po(settleBillParam);
            list.add(settleBill);
        });
        return list;
   }

    public List<SettleBillBO> poList2BoList(List<SettleBill> list) {
        List<SettleBillBO> settleBillBos = new ArrayList<>(list.size());
        for (SettleBill settleBill : list) {
            settleBillBos.add(this.po2bo(settleBill));
        }
        return settleBillBos;
    }

    public SettleBillBO po2bo(SettleBill settleBill) {
        SettleBillBO settleBillBO = new SettleBillBO();
        BeanUtils.copyProperties(settleBill, settleBillBO);
        return settleBillBO;
    }
}