package com.tarena.tp.luban.settle.server.web.producer;

import com.tarena.tp.luban.settle.server.service.EventService;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
public class EventServiceImpl implements EventService {
    @Autowired
    private RocketMQTemplate rocketTemplate;
    @Override
    public void sendSettledEvent(String orderNo) {
        //封装消息
        Message<String> message = MessageBuilder.withPayload(orderNo).build();
        //异步发送
        rocketTemplate.asyncSend("order_complete_topic", message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("发送成功");
            }
            @Override
            public void onException(Throwable throwable) {
                System.out.println("发送失败");
            }
        });
    }
}
