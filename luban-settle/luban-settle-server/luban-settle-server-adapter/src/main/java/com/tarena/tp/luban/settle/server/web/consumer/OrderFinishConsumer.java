package com.tarena.tp.luban.settle.server.web.consumer;

import com.tarena.tp.luban.order.server.protocol.dto.OrderMqDTO;
import com.tarena.tp.luban.settle.server.service.SettleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@RocketMQMessageListener(topic="order_finish_topic",consumerGroup ="settle_group1" )
@Slf4j
public class OrderFinishConsumer implements RocketMQListener<OrderMqDTO> {
    @Autowired
    private SettleService settleService;
    @Override
    public void onMessage(OrderMqDTO message) {
        try{
            settleService.settle(message);
        }catch (Exception e){
            log.error("结算订单:{},出现异常:{}",message.getOrderNo(),e.getMessage());
        }
    }
}
