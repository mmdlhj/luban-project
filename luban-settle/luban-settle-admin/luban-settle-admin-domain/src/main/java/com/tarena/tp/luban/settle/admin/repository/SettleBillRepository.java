/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.settle.admin.repository;
import com.tarena.tp.luban.settle.admin.bo.SettleBillBO;
import com.tarena.tp.luban.settle.admin.protocol.param.SettleBillParam;
import com.tarena.tp.luban.settle.admin.protocol.query.SettleBillQuery;
import java.util.List;

public interface SettleBillRepository {
    Long save(SettleBillParam settleBillParam);

    Integer delete(Long settleBillId);

    SettleBillBO getSettleBill(Long settleBillId);

    Long getSettleBillCount(SettleBillQuery settleBillQuery);

    List<SettleBillBO> querySettleBills(SettleBillQuery settleBillBOQuery);

    Integer disable(String bankIds);

    Integer enable(String bankIds);
}