package com.tarena.tp.luban.demand.admin.dao;

import com.tarena.tp.luban.demand.po.ProviderAuditLog;

import java.util.List;

public interface ProviderAuditLogDAO {

    Integer insert(ProviderAuditLog providerAuditLog);

    ProviderAuditLog getEntityByProviderId(Long providerId);

    List<ProviderAuditLog> getAuditLogByProviderId(Long providerId);
}
