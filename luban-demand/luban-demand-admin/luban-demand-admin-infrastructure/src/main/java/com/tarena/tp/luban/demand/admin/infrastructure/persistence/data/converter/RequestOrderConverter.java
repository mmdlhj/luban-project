/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.infrastructure.persistence.data.converter;


import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.demand.admin.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.admin.dao.query.RequestOrderDBPagerQuery;
import com.tarena.tp.luban.demand.admin.protocol.param.RequestOrderParam;
import com.tarena.tp.luban.demand.admin.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.po.RequestOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RequestOrderConverter {

    public RequestOrderDBPagerQuery toDbPagerQuery(RequestOrderQuery requestOrderQuery){
        if (requestOrderQuery == null) {
            return new RequestOrderDBPagerQuery();
        }
        RequestOrderDBPagerQuery requestOrderDBPagerQuery = new RequestOrderDBPagerQuery();
        BeanUtils.copyProperties(requestOrderQuery, requestOrderDBPagerQuery);
        return requestOrderDBPagerQuery;
    }

    public RequestOrder param2po(RequestOrderParam param) {
        RequestOrder requestOrder = new RequestOrder();
        BeanUtils.copyProperties(param, requestOrder);

        LoginUser loginUser= SecurityContext.getLoginToken();

        requestOrder.setGmtCreate(System.currentTimeMillis());
        requestOrder.setGmtModified(requestOrder.getGmtCreate());
        requestOrder.setStatus(1);

        requestOrder.setCreateUserId(99L);
        requestOrder.setModifiedUserId(99L);
        requestOrder.setCreateUserName("root");
        requestOrder.setModifiedUserName("root");

        return requestOrder;
    }

    public RequestOrderBO po2bo(RequestOrder requestOrder) {
        RequestOrderBO requestOrderBO = new RequestOrderBO();
        BeanUtils.copyProperties(requestOrder, requestOrderBO);
        return requestOrderBO;
    }

    public List<RequestOrderBO> poList2BoList(List<RequestOrder> list) {
        List<RequestOrderBO> requestOrderBos = new ArrayList<>(list.size());
        for (RequestOrder requestOrder : list) {
            requestOrderBos.add(this.po2bo(requestOrder));
        }
        return requestOrderBos;
    }


}
