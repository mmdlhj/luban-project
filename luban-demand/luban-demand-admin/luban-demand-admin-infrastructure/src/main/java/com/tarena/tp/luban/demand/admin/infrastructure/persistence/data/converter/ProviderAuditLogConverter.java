/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.infrastructure.persistence.data.converter;


import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.demand.admin.bo.ProviderAuditLogBO;
import com.tarena.tp.luban.demand.admin.protocol.param.ProviderAuditLogParam;
import com.tarena.tp.luban.demand.po.ProviderAuditLog;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Component
public class ProviderAuditLogConverter {

    public ProviderAuditLog param2po(ProviderAuditLogParam param) {
        ProviderAuditLog providerAuditLog = new ProviderAuditLog();
        BeanUtils.copyProperties(param, providerAuditLog);
        providerAuditLog.setOperateName("审核");
        LoginUser loginUser = SecurityContext.getLoginToken();
        providerAuditLog.setUserName(loginUser.getUserName());
        providerAuditLog.setUserId(loginUser.getUserId());
        Long now = System.currentTimeMillis();
        providerAuditLog.setOperateTime(now);
        providerAuditLog.setGmtCreate(now);
        providerAuditLog.setGmtModified(now);
        providerAuditLog.setCreateUserId(loginUser.getUserId());
        providerAuditLog.setModifiedUserId(loginUser.getUserId());
        providerAuditLog.setCreateUserName(loginUser.getUserName());
        providerAuditLog.setModifiedUserName(loginUser.getUserName());
        return providerAuditLog;
    }

    public ProviderAuditLogBO po2bo(ProviderAuditLog providerAuditLog) {
        ProviderAuditLogBO providerAuditLogBO = new ProviderAuditLogBO();
        BeanUtils.copyProperties(providerAuditLog, providerAuditLogBO);
        return providerAuditLogBO;
    }


    public List<ProviderAuditLogBO> pos2bos(List<ProviderAuditLog> providerAuditLog) {
        if (CollectionUtils.isEmpty(providerAuditLog)) {
            return Collections.emptyList();
        }
        List<ProviderAuditLogBO> result = new ArrayList<>();
        providerAuditLog.forEach(po-> result.add(po2bo(po)));
        return result;
    }
}
