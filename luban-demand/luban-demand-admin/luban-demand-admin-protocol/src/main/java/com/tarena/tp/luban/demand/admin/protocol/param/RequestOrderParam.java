/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.admin.protocol.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderParam {

 @ApiModelProperty("用户下单姓名")
 private String userName;

 @ApiModelProperty("用户手机号")
 private String userPhone;

 @ApiModelProperty("订单名")
 private String orderName;

 @ApiModelProperty("需求单编号")
 private String requestOrderNo;

 @ApiModelProperty("订单类型")
 private Integer categoryId;

 @ApiModelProperty("订单单价")
 private Integer orderAmount;

 @ApiModelProperty("最晚交付时间")
 private Long serviceTime;

 @ApiModelProperty("上门详细地址")
 private String address;


}