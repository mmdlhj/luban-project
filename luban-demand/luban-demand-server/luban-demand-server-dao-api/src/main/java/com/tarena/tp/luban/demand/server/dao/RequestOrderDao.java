/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.server.dao;

import com.tarena.tp.luban.demand.po.RequestOrder;
import com.tarena.tp.luban.demand.server.dao.param.GrabOrderParam;
import com.tarena.tp.luban.demand.server.dao.query.RequestOrderDBPagerQuery;

import java.util.List;

public interface RequestOrderDao {

    Integer insert(RequestOrder requestOrder);

    Integer grabOrder(String requestOrderNo);

    Integer cancelGrabOrder(String requestOrderNo);

    RequestOrder getRequestOrderByOrderNo(String requestOrderNo);

    List<RequestOrder> queryRequestOrders(RequestOrderDBPagerQuery requestOrderDBPagerQuery);

    Long countRequestOrder(RequestOrderDBPagerQuery requestOrderDBPagerQuery);

    Integer grabOrderCas(GrabOrderParam grabOrderParam);

    Integer returnOrder(GrabOrderParam grabOrderParam);
}