package com.tarena.tp.luban.demand.server.service;

import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.luban.demand.server.bo.AccessProviderBO;
import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.repository.AccessProviderRepository;
import com.tarena.tp.luban.demand.server.repository.RequestOrderRepository;
import com.tedu.inn.protocol.ListRecordTotalBO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DemandService {
    @Autowired
    private RequestOrderRepository requestOrderRepository;
    public ListRecordTotalBO<RequestOrderBO> demandList(RequestOrderQuery requestOrderQuery) {
        //1.远程调用Worker-Server将当抢查询query封装完成
       /* replenishQuery(requestOrderQuery);*/
        //2.查询total select count(*) from request_order where grab_status=0 and category_id in (4) and
        // area_id in (3) order by gmt_create
        Long total = requestOrderRepository.getRequestOrderCount(requestOrderQuery);
        List<RequestOrderBO> requestOrderBOS=null;
        if (total>0){
            //说明有值
            //3.查询list select * from request_order where grab_status=0 and category_id in (4) and
            //area_id in (3) order by gmt_create limit (#{pageNO}-1)*#{pageSize},#{pageSize}
            requestOrderBOS=requestOrderRepository.queryRequestOrders(requestOrderQuery);
            calculateViewAmount(requestOrderBOS);
        }
        return new ListRecordTotalBO<>(requestOrderBOS,total);
    }

    /**
     * 分润 profitScale 固定20.00 20% 利用orderAmount计算view
     * @param requestOrderBOS
     */
    @Autowired
    private AccessProviderRepository accessProviderRepository;
    private void calculateViewAmount(List<RequestOrderBO> requestOrderBOS) {
        for (RequestOrderBO requestOrderBO : requestOrderBOS) {
            //TODO 调用provider厂商的仓储层实现查询实际的分润
            AccessProviderBO accessProvider = accessProviderRepository.getAccessProvider(requestOrderBO.getProviderId().longValue());
            requestOrderBO.setProfitScale(accessProvider.getProfitScale());
            //利用orderAmount计算viewOrderAmount orderAmount*(100-profitScale)/100
            //师傅的分润比例 80% 0.8
            BigDecimal workerPercent=
                    new BigDecimal(100).subtract(requestOrderBO.getProfitScale())
                    .divide(new BigDecimal(100));
            log.info("师傅分润比例:{}",workerPercent);
            Long viewOrderAmount=
                    new BigDecimal(requestOrderBO.getOrderAmount()).multiply(workerPercent).longValue();
            requestOrderBO.setViewOrderAmount(viewOrderAmount);
            log.info("订单原价:{},师傅展示价格:{}",requestOrderBO.getOrderAmount(),viewOrderAmount);
        }
    }
    private void replenishQuery(RequestOrderQuery requestOrderQuery) {
        /*Long userId = SecurityContext.getLoginToken().getUserId();
        WorkerDTO workerDTO = workerApi.queryWorker(userId);
        requestOrderQuery.setOrderCategoryIds(workerDTO.getCategoryIds());
        requestOrderQuery.setAreaIds(workerDTO.getAreaIds());*/
        //TODO 远程调用封装
       /* List<Integer> areaIds=new ArrayList<>();
        areaIds.add(3);
        List<Integer> categoryIds=new ArrayList<>();
        categoryIds.add(4);
        requestOrderQuery.setAreaIds(areaIds);
        requestOrderQuery.setOrderCategoryIds(categoryIds);*/
    }

    public Boolean grabOrder(String requestOrderNo) {
        Integer result=requestOrderRepository.grabOrder(requestOrderNo);
        //抢到需求单,result=1 没抢到 result=0
        return result==1;
    }

    public Boolean returnOrder(String requestOrderNo) {
        Integer integer = requestOrderRepository.returnOrder(requestOrderNo);
        //归还成功 update执行成功 integer=1
        return integer==1;
    }
}
