/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.server.bo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AccessProviderBO {
 private Long id;
 private String ProviderName;
 private String privateKey;
 private String publicKey;
 private String description;
 private String address;
 private String businessLicense;
 private String principal;
 private String principalTel;
 private BigDecimal profitScale;
 private Integer status;
 private Integer auditStatus;
 private String createUserName;
 private Long createUserId;
 private Long modifiedUserId;
 private String modifiedUserName;
 private Long gmtModified;
 private Long gmtCreate;

}