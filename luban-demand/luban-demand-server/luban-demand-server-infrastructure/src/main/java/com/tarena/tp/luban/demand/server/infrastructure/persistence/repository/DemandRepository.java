package com.tarena.tp.luban.demand.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.demand.po.RequestOrder;
import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.dao.RequestOrderDao;
import com.tarena.tp.luban.demand.server.dao.param.GrabOrderParam;
import com.tarena.tp.luban.demand.server.dao.query.RequestOrderDBPagerQuery;
import com.tarena.tp.luban.demand.server.infrastructure.persistence.data.converter.RequestOrderConverter;
import com.tarena.tp.luban.demand.server.protocol.param.RequestOrderParam;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.repository.RequestOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class DemandRepository implements RequestOrderRepository {
    @Autowired(required = false)
    private RequestOrderDao requestOrderDao;
    @Autowired
    private RequestOrderConverter requestOrderConverter;
    @Override
    public Long getRequestOrderCount(RequestOrderQuery requestOrderQuery) {
        //转化成dbquery
        RequestOrderDBPagerQuery dbQuery =
                requestOrderConverter.toDbPagerQuery(requestOrderQuery);
        //持久层调用
        return requestOrderDao.countRequestOrder(dbQuery);
    }
    @Override
    public List<RequestOrderBO> queryRequestOrders(RequestOrderQuery requestOrderQuery) {
        //转化成dbquery
        RequestOrderDBPagerQuery dbQuery =
                requestOrderConverter.toDbPagerQuery(requestOrderQuery);
        //持久层调用
        List<RequestOrder> requestOrders = requestOrderDao.queryRequestOrders(dbQuery);
        //返回值转化
        return requestOrderConverter.poList2BoList(requestOrders);
    }
    @Override
    public Long save(RequestOrderParam requestOrderParam) {
        return null;
    }

    @Override
    public Integer grabOrder(String requestOrderNo) {
        //1. 查询当前需求单的version
        RequestOrder requestOrder = requestOrderDao.getRequestOrderByOrderNo(requestOrderNo);
        Integer result=0;
        //2. 使用版本号 grab_status=0 request_order_no=#{requestOrderNo} 做更新
        if (requestOrder!=null){
            //update request_order set grab_status=1,version=version+1
            //where grab_status=0 and request_order_no=#{} and version=#{}
            GrabOrderParam grabOrderParam=new GrabOrderParam();
            grabOrderParam.setRequestOrderNO(requestOrderNo);
            grabOrderParam.setVersion(requestOrder.getVersion());
            result = requestOrderDao.grabOrderCas(grabOrderParam);
        }
        return result;
    }

    @Override
    public Integer cancelGrabOrder(String requestOrderNo) {
        return null;
    }

    @Override
    public RequestOrderBO getRequestOrder(String requestOrderNo) {
        return null;
    }

    @Override
    public Integer returnOrder(String requestOrderNo) {
        //update request_order set grab_status=0,version=verson+1 where request_order_no=#{}
        GrabOrderParam grabOrderParam=new GrabOrderParam();
        grabOrderParam.setRequestOrderNO(requestOrderNo);
        //调用持久层dao
        return requestOrderDao.returnOrder(grabOrderParam);
    }
}
