package com.tarena.tp.luban.demand.server.infrastructure.persistence.repository;

import com.tarena.tp.luban.demand.po.AccessProvider;
import com.tarena.tp.luban.demand.server.bo.AccessProviderBO;
import com.tarena.tp.luban.demand.server.dao.AccessProviderDAO;
import com.tarena.tp.luban.demand.server.infrastructure.persistence.data.converter.AccessProviderConverter;
import com.tarena.tp.luban.demand.server.repository.AccessProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProviderRepositoryImpl implements AccessProviderRepository {
    @Autowired
    private AccessProviderDAO accessProviderDAO;
    @Autowired
    private AccessProviderConverter accessProviderConverter;
    @Override
    public AccessProviderBO getAccessProvider(Long accessProviderId) {
        AccessProvider entity = accessProviderDAO.getEntity(accessProviderId);

        return accessProviderConverter.po2bo(entity);
    }
}
