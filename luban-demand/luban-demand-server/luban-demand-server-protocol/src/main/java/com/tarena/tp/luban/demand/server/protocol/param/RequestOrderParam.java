/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.demand.server.protocol.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RequestOrderParam {

    @ApiModelProperty("用户下单姓名")
    String userName;

    @ApiModelProperty("用户手机号")
    String userPhone;

    @ApiModelProperty("订单名")
    String orderName;

    @ApiModelProperty("需求单编号")
    String requestOrderNo;

    @ApiModelProperty("区域id")
    Integer areaId;

    @ApiModelProperty("订单类型")
    Integer categoryId;

    @ApiModelProperty("订单单价")
    Integer orderAmount;

    @ApiModelProperty("最晚交付时间")
    Long serviceTime;

    @ApiModelProperty("上门详细地址")
    String address;


}