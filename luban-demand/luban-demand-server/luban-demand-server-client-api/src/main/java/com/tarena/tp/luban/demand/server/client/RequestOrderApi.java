package com.tarena.tp.luban.demand.server.client;

/**
 * @Author: goodman
 * @Date: 2023/4/25 15:02
 */
public interface RequestOrderApi {
    Boolean grabOrder(String requestOrderNo);

    Boolean returnOrder(String requestOrderNo);
}
