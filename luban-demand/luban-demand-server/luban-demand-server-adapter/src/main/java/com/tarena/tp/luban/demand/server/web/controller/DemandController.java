package com.tarena.tp.luban.demand.server.web.controller;

import com.tarena.tp.luban.demand.server.bo.RequestOrderBO;
import com.tarena.tp.luban.demand.server.protocol.query.RequestOrderQuery;
import com.tarena.tp.luban.demand.server.service.DemandService;
import com.tarena.tp.luban.demand.server.web.assemble.RequestOrderAssemble;
import com.tarena.tp.luban.demand.server.web.vo.RequestOrderListItemVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.model.Result;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemandController {
    @Autowired
    private RequestOrderAssemble requestOrderAssemble;
    @Autowired
    private DemandService demandService;
    /**
     * 根据 师傅服务区域areaIds 和服务种类categoryIds
     * 查询 未抢状态的需求单 grab_status 0 未抢 1 已抢
     */
    @PostMapping("demand/order/search")
    public Result<PagerResult<RequestOrderListItemVO>>  demandList(
            @RequestBody RequestOrderQuery requestOrderQuery){
        // 业务调用查询 一个有total 和list的对象 TODO
        ListRecordTotalBO<RequestOrderBO> demandBOS=
                demandService.demandList(requestOrderQuery);
        // 转化
        PagerResult<RequestOrderListItemVO> pageResult =
                requestOrderAssemble.assemblePagerResult(demandBOS, requestOrderQuery);
        // 包装返回
        return new Result(pageResult);
    }
}
