package com.tarena.tp.luban.demand.server.web.rpc;

import com.tarena.tp.luban.demand.server.client.RequestOrderApi;
import com.tarena.tp.luban.demand.server.service.DemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("demandServerRpcImpl")
public class DemandRpcImpl implements RequestOrderApi {
    @Autowired
    private DemandService demandService;
    @Override
    public Boolean grabOrder(String requestOrderNo) {
        //TODO 使用redis抢锁 抢到锁的,才执行 业务层.
        return demandService.grabOrder(requestOrderNo);
    }

    @Override
    public Boolean returnOrder(String requestOrderNo) {
        return demandService.returnOrder(requestOrderNo);
    }
}
