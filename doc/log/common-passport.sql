/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.33-log : Database - common_passport
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`common_passport` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `common_passport`;

/*Table structure for table `cmp_admin` */

DROP TABLE IF EXISTS `cmp_admin`;

CREATE TABLE `cmp_admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `avator` varchar(64) DEFAULT NULL COMMENT '用户描述',
  `nickname` varchar(16) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT '1',
  `enable` tinyint(1) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `cmp_admin` */

insert  into `cmp_admin`(`id`,`username`,`password`,`avator`,`nickname`,`phone`,`email`,`is_delete`,`enable`,`gmt_create`,`gmt_modified`) values (1,'guest','$e21b0ba4dcc.3a944f9.b6aa5097a4bdbfe65962e00c5b7dfb2509f783893',NULL,'nickname2','16639644272','1826265849@qq.com',1,1,'2022-10-21 19:38:00','2022-11-24 20:59:33');
insert  into `cmp_admin`(`id`,`username`,`password`,`avator`,`nickname`,`phone`,`email`,`is_delete`,`enable`,`gmt_create`,`gmt_modified`) values (12,'guest1','$e21y0babdac6319c4f90b.af5d95a4b9b8e25b64ec075975f42.08fe8e803',NULL,'liyuan','16639644277','1826265847@qq.com',1,1,'2022-12-10 14:45:01','2022-12-10 14:45:01');
insert  into `cmp_admin`(`id`,`username`,`password`,`avator`,`nickname`,`phone`,`email`,`is_delete`,`enable`,`gmt_create`,`gmt_modified`) values (13,'test000','$e21a00aad6cb3a9.409bb9ac589.a4b0bfe35a62ec065c75ff2202f389853',NULL,'大王','18500039998','xiao@qq.com',1,1,'2023-07-26 01:08:09','2023-07-26 01:08:09');

/*Table structure for table `cmp_login_log` */

DROP TABLE IF EXISTS `cmp_login_log`;

CREATE TABLE `cmp_login_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `nickname` varchar(50) DEFAULT NULL,
  `ip` varchar(50) DEFAULT NULL,
  `user_agent` varchar(50) DEFAULT NULL,
  `gmt_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `cmp_login_log` */

/*Table structure for table `cmp_login_log_detail` */

DROP TABLE IF EXISTS `cmp_login_log_detail`;

CREATE TABLE `cmp_login_log_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login_id` bigint(20) DEFAULT NULL,
  `state` tinyint(1) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `cmp_login_log_detail` */

/*Table structure for table `cmp_operate_log_detail` */

DROP TABLE IF EXISTS `cmp_operate_log_detail`;

CREATE TABLE `cmp_operate_log_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `admin_id` bigint(20) DEFAULT NULL,
  `state` tinyint(1) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `gmt_operate` datetime DEFAULT NULL,
  `request_parameter` varchar(255) DEFAULT NULL,
  `operate_method` varchar(255) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*Data for the table `cmp_operate_log_detail` */

insert  into `cmp_operate_log_detail`(`id`,`admin_id`,`state`,`detail`,`gmt_operate`,`request_parameter`,`operate_method`,`is_delete`) values (1,13,1,'','2023-07-26 01:08:09','{\"password\":\"{protected}\",\"phone\":\"18500039998\",\"nickname\":\"大王\",\"actPassword\":\"123456\",\"email\":\"xiao@qq.com\",\"username\":\"test000\"}','addNewUser',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
