package com.tarena.tp.luban.account.server.param;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PaymentParam implements Serializable {

    Long userId;

    String orderNo;

    Long totalAmount;
}
