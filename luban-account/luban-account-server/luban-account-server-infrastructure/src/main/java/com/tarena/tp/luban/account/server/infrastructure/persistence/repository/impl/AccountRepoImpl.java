package com.tarena.tp.luban.account.server.infrastructure.persistence.repository.impl;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.po.Account;
import com.tarena.tp.luban.account.server.dao.AccountDAO;
import com.tarena.tp.luban.account.server.infrastructure.persistence.data.converter.AccountConverter;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccountRepoImpl implements AccountRepository {
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private AccountConverter accountConverter;
    @Override
    public Long create(AccountParam accountCreateParam) {
        Account account = accountConverter.param2po(accountCreateParam);
        return accountDAO.insert(account);
    }

    @Override
    public AccountBO getAccountByUserId(Long userId) {
        //持久层查询 返回 do
        Account account = accountDAO.getAccountByUserId(userId);
        //转化 do2bo
        return accountConverter.po2bo(account);
    }

    @Override
    public Long updateAmount(PaymentParam param) {
        //转化
        Account account = accountConverter.param2po(param);
        //调用update
        return accountDAO.updateAmount(account);
    }
}
