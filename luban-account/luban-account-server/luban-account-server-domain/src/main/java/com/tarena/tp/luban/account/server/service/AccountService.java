package com.tarena.tp.luban.account.server.service;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepository;
    public Long create(AccountParam accountParam) {
        //1. 幂等考虑,如果一个userId已经存在账户,就不创建
        AccountBO accountBO = accountRepository.getAccountByUserId(accountParam.getUserId());
        if (accountBO==null || accountBO.getTotalAmount()==null){
            //userId绑定的账户不存在的
            return accountRepository.create(accountParam);
        }
        //如果返回是0,说明装创建失败,如果返回是1说明成功 如果返回-1 说明已存在.
        return -1L;
    }

    public AccountBO getAccountByUserId(Long userId) {
        return accountRepository.getAccountByUserId(userId);
    }

    public Long mockPay(PaymentParam param) {
        // update account set amount=amount+#{totalAmount} where user_id=#{userId}
        return accountRepository.updateAmount(param);
    }
}
