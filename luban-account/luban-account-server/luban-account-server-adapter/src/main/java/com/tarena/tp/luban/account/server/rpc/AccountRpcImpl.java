package com.tarena.tp.luban.account.server.rpc;

import com.tarena.tp.luban.account.bo.AccountBO;
import com.tarena.tp.luban.account.server.assemble.AccountAssemble;
import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.dto.AccountDTO;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.account.server.param.PaymentParam;
import com.tarena.tp.luban.account.server.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountRpcImpl implements AccountApi {
    @Autowired
    private AccountService accountService;
    //师傅审核通过 调用创建账户
    @Override
    public Long create(AccountParam accountCreateParam) {
        return accountService.create(accountCreateParam);
    }
    @Autowired
    private AccountAssemble accountAssemble;
    @Override
    public AccountDTO getAccountByUserId(Long userId) {
        AccountBO accountBO=accountService.getAccountByUserId(userId);
        return accountAssemble.assembleAccountDTO(accountBO);
    }

    @Override
    public Long mockPayment(PaymentParam param) {
        return accountService.mockPay(param);
    }
}
