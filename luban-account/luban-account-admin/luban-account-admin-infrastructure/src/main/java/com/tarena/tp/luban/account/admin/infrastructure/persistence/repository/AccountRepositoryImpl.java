/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.infrastructure.persistence.repository;

import com.tarena.tp.luban.account.admin.dao.AccountDAO;
import com.tedu.inn.protocol.enums.StatusRecord;
import com.tedu.inn.protocol.dao.StatusCriteria;
import com.tarena.tp.luban.account.admin.infrastructure.persistence.data.converter.AccountConverter;
import com.tarena.tp.luban.account.po.Account;
import com.tarena.tp.luban.account.admin.bo.AccountBO;
import com.tarena.tp.luban.account.admin.protocol.param.AccountParam;
import com.tarena.tp.luban.account.admin.repository.AccountRepository;
import com.tarena.tp.luban.account.admin.protocol.query.AccountQuery;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountRepositoryImpl implements AccountRepository {
    @Autowired
    private AccountConverter accountConverter;

    @Autowired
    private AccountDAO accountDao;

    @Override public Long save(AccountParam accountParam) {
        Account account = this.accountConverter.param2po(accountParam);
        if (account.getId() != null) {
            this.accountDao.update(account);
            return account.getId();
        }
        this.accountDao.insert(account);
        return  account.getId();
    }

    @Override public Integer delete(Long accountId) {
        return this.accountDao.delete(accountId);
    }

    @Override public Integer disable(String accountIds) {
        StatusCriteria statusCriteria = new StatusCriteria(accountIds, StatusRecord.DISABLE.getStatus());
        this.accountConverter.convertStatus(statusCriteria);
        return this.accountDao.changeStatus(statusCriteria);
    }

    @Override public Integer enable(String accountIds) {
        StatusCriteria statusCriteria = new StatusCriteria(accountIds, StatusRecord.ENABLE.getStatus());
        this.accountConverter.convertStatus(statusCriteria);
        return this.accountDao.changeStatus(statusCriteria);
    }

    @Override public AccountBO getAccount(Long accountId) {
        Account account = this.accountDao.getEntity(accountId);
        return this.accountConverter.po2bo(account);
    }

    @Override public List<AccountBO> queryAccounts(
        AccountQuery accountQuery) {
        List<Account> accountList = this.accountDao.queryAccounts(this.accountConverter.toDbPagerQuery(accountQuery));
        return this.accountConverter.poList2BoList(accountList);
    }

    @Override public Long getAccountCount(AccountQuery accountQuery) {
        return this.accountDao.countAccount(this.accountConverter.toDbPagerQuery(accountQuery));
    }
}