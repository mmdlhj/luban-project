/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.manager;

import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tarena.tp.luban.account.admin.bo.BankCardBO;
import com.tarena.tp.luban.account.admin.repository.BankCardRepository;
import com.tarena.tp.luban.account.admin.protocol.param.BankCardParam;
import com.tarena.tp.luban.account.admin.protocol.query.BankCardQuery;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class BankCardService {
    @Autowired
    private BankCardRepository bankCardRepository;

    private void validateSaveBankCard(
        BankCardParam bankCardParam) throws BusinessException {
        //Asserts.isTrue(StringUtility.isNullOrEmpty(bankCardParam.getName()), SecurityAdminError.NAME_IS_EMPTY, BankCardSuffix.name);
    }

    public Long saveBankCard(
        BankCardParam bankCardParam) throws BusinessException {
        this.validateSaveBankCard(bankCardParam);
        return this.bankCardRepository.save(bankCardParam);
    }

    public Integer deleteBankCard(Long bankCardId) throws BusinessException {
        //Asserts.isTrue(StringUtility.isNullOrEmpty(bankCardId), SecurityAdminError.BankCard_ID_IS_EMPTY);
        return this.bankCardRepository.delete(bankCardId);
    }

    public Integer enableBankCard(String bankCardIds) throws BusinessException {
        //Asserts.isTrue(StringUtils.isEmpty(bankIds), BankCardAdminError.ID_IS_EMPTY);
        return this.bankCardRepository.enable(bankCardIds);
    }

    public Integer disableBankCard(String bankCardIds) throws BusinessException {
        //Asserts.isTrue(StringUtils.isEmpty(bankCardIds), BankCardAdminError.ID_IS_EMPTY);
        return this.bankCardRepository.disable(bankCardIds);
    }

    public ListRecordTotalBO<BankCardBO> queryAllBankCard() {
        return queryBankCard(null);
    }

    public ListRecordTotalBO<BankCardBO> queryBankCard(
        BankCardQuery bankCardQuery) {
        Long totalRecord = this.bankCardRepository.getBankCardCount(bankCardQuery);
        List<BankCardBO> bankCardBoList = null;
        if (totalRecord > 0) {
            bankCardBoList = this.bankCardRepository.queryBankCards(bankCardQuery);
        }
        return new ListRecordTotalBO<>(bankCardBoList, totalRecord);
    }

    public BankCardBO getBankCard(
        Long bankCardId) throws BusinessException {
        //Asserts.isTrue(bankCardId == null, BankCardAdminError.IS_EMPTY);
        return this.bankCardRepository.getBankCard(bankCardId);
    }
}