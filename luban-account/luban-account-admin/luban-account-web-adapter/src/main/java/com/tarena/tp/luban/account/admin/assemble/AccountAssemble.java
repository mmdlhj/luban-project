/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.assemble;

import com.tarena.tp.luban.account.admin.bo.AccountBO;
import com.tarena.tp.luban.account.admin.protocol.vo.AccountVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.pager.PagerResult;
import com.tedu.inn.protocol.pager.SimplePagerQuery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class AccountAssemble {
    public AccountVO assembleBO2VO(AccountBO bo) {
        AccountVO account = new AccountVO();
        BeanUtils.copyProperties(bo, account);
        return account;
    }

    public List<AccountVO> boListAssembleVOList(List<AccountBO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<AccountVO> accountVOList = new ArrayList<>(list.size());
        for (AccountBO accountBo : list) {
            AccountVO accountVo = this.assembleBO2VO(accountBo);
            accountVOList.add(accountVo);
        }
        return accountVOList;
    }

    public PagerResult<AccountVO> assemblePagerResult(ListRecordTotalBO<AccountBO> accountListTotalRecord,
        SimplePagerQuery accountQuery) {
        List<AccountVO> accountVOList = this.boListAssembleVOList(accountListTotalRecord.getList());
        PagerResult<AccountVO> pagerResult = new PagerResult<>(accountQuery);
        pagerResult.setObjects(accountVOList);
        pagerResult.setTotal(accountListTotalRecord.getTotal());
        return pagerResult;
    }
}