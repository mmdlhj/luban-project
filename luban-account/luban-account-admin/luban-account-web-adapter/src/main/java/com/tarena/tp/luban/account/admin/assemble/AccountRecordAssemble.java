/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.assemble;

import com.tarena.tp.luban.account.admin.bo.AccountRecordBO;
import com.tarena.tp.luban.account.admin.protocol.vo.AccountRecordVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.pager.PagerResult;
import com.tedu.inn.protocol.pager.SimplePagerQuery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class AccountRecordAssemble {
    public AccountRecordVO assembleBO2VO(AccountRecordBO bo) {
        AccountRecordVO accountRecord = new AccountRecordVO();
        BeanUtils.copyProperties(bo, accountRecord);
        return accountRecord;
    }

    public List<AccountRecordVO> boListAssembleVOList(List<AccountRecordBO> list) {
        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }
        List<AccountRecordVO> accountRecordVOList = new ArrayList<>(list.size());
        for (AccountRecordBO accountRecordBo : list) {
            AccountRecordVO accountRecordVo = this.assembleBO2VO(accountRecordBo);
            accountRecordVOList.add(accountRecordVo);
        }
        return accountRecordVOList;
    }

    public PagerResult<AccountRecordVO> assemblePagerResult(ListRecordTotalBO<AccountRecordBO> accountRecordListTotalRecord,
        SimplePagerQuery accountRecordQuery) {
        List<AccountRecordVO> accountRecordVOList = this.boListAssembleVOList(accountRecordListTotalRecord.getList());
        PagerResult<AccountRecordVO> pagerResult = new PagerResult<>(accountRecordQuery);
        pagerResult.setObjects(accountRecordVOList);
        pagerResult.setTotal(accountRecordListTotalRecord.getTotal());
        return pagerResult;
    }
}