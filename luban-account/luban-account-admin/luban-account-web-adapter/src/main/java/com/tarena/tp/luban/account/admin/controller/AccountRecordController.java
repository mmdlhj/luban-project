/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.controller;

import com.tarena.tp.luban.account.admin.assemble.AccountRecordAssemble;
import com.tarena.tp.luban.account.admin.bo.AccountRecordBO;
import com.tarena.tp.luban.account.admin.manager.AccountRecordService;
import com.tarena.tp.luban.account.admin.protocol.param.AccountRecordParam;
import com.tarena.tp.luban.account.admin.protocol.query.AccountRecordQuery;
import com.tarena.tp.luban.account.admin.protocol.vo.AccountRecordVO;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("admin/accountRecord")
public class AccountRecordController {
    @Autowired
    private AccountRecordService accountRecordService;

    @Autowired
    private AccountRecordAssemble accountRecordAssemble;

    @PostMapping("search")
    @ResponseBody
    public PagerResult<AccountRecordVO> queryAccountRecords(AccountRecordQuery accountRecordQuery) {
        ListRecordTotalBO<AccountRecordBO> accountRecordListTotalRecord = this.accountRecordService.queryAccountRecord(accountRecordQuery);
        return this.accountRecordAssemble.assemblePagerResult(accountRecordListTotalRecord, accountRecordQuery);
    }

    @PostMapping("save")
    @ResponseBody
    public Long saveAccountRecord(AccountRecordParam accountRecordParam) throws BusinessException {
        return this.accountRecordService.saveAccountRecord(accountRecordParam);
    }

    @GetMapping("get")
    public AccountRecordVO getAccountRecord(Long accountRecordId) throws BusinessException {
        AccountRecordBO accountRecordBo = accountRecordService.getAccountRecord(accountRecordId);
        return this.accountRecordAssemble.assembleBO2VO(accountRecordBo);
    }

    @PostMapping("delete")
    public Integer deleteAccountRecord(Long id) throws BusinessException {
        return this.accountRecordService.deleteAccountRecord(id);
    }

    @PostMapping("enable")
    public Integer enableAccountRecord(String ids) throws BusinessException {
        return this.accountRecordService.enableAccountRecord(ids);
    }

    @PostMapping("disable")
    public Integer disableAccountRecord(String ids) throws BusinessException {
        return this.accountRecordService.disableAccountRecord(ids);
    }
}