/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.account.admin.protocol.param;

public class AccountParam{
    private Long id; 
private Long userId; 
private Long settlingAmount; 
private Long totalAmount; 
private String createUserName; 
private Long createUserId; 
private Long modifiedUserId; 
private String modifiedUserName; 
private Long gmtCreate; 
private Long gmtModified; 
private Integer status; 
public Long getId(){
 return this.id;
}
public void setId(Long id){
this.id=id;
}
public Long getUserId(){
 return this.userId;
}
public void setUserId(Long userId){
this.userId=userId;
}
public Long getSettlingAmount(){
 return this.settlingAmount;
}
public void setSettlingAmount(Long settlingAmount){
this.settlingAmount=settlingAmount;
}
public Long getTotalAmount(){
 return this.totalAmount;
}
public void setTotalAmount(Long totalAmount){
this.totalAmount=totalAmount;
}
public String getCreateUserName(){
 return this.createUserName;
}
public void setCreateUserName(String createUserName){
this.createUserName=createUserName;
}
public Long getCreateUserId(){
 return this.createUserId;
}
public void setCreateUserId(Long createUserId){
this.createUserId=createUserId;
}
public Long getModifiedUserId(){
 return this.modifiedUserId;
}
public void setModifiedUserId(Long modifiedUserId){
this.modifiedUserId=modifiedUserId;
}
public String getModifiedUserName(){
 return this.modifiedUserName;
}
public void setModifiedUserName(String modifiedUserName){
this.modifiedUserName=modifiedUserName;
}
public Long getGmtCreate(){
 return this.gmtCreate;
}
public void setGmtCreate(Long gmtCreate){
this.gmtCreate=gmtCreate;
}
public Long getGmtModified(){
 return this.gmtModified;
}
public void setGmtModified(Long gmtModified){
this.gmtModified=gmtModified;
}
public Integer getStatus(){
 return this.status;
}
public void setStatus(Integer status){
this.status=status;
}

}