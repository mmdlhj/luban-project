package com.tarena.tp.luban.worker.server.web.controller;

import com.tarena.tp.luban.worker.server.bo.WorkerBO;
import com.tarena.tp.luban.worker.server.dto.param.WorkerCreateParam;
import com.tarena.tp.luban.worker.server.manager.WorkerService;
import com.tarena.tp.luban.worker.server.web.assemble.WorkerAssemble;
import com.tarena.tp.luban.worker.server.web.vo.WorkerVO;
import com.tedu.inn.protocol.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(value = "worker",tags = "师傅接口")
@Slf4j
@Validated
@RestController
@RequestMapping("worker")
public class WorkerController {

    @Resource
    private WorkerService workerService;

    @Resource
    private WorkerAssemble workerAssemble;

    /**
     * 用户身份认证
     */
    //@PostMapping(value = "idcard/check")
    //@ApiOperation("身份认证")
    //public IdCardVO IdCard(@RequestBody List<IdCardParam> idCardParam) throws  Exception{
     //   IdCardBO idCard = workerService.checkIdCard(idCardParam);
     //   return this.workerAssemble.assembleBO2VO(idCard);
    //}

    @PostMapping(value = "create")
    @ResponseBody
    @ApiOperation(value = "师傅入驻")
    public Long createWorker(@RequestBody WorkerCreateParam workerParam) throws BusinessException {
        return this.workerService.create(workerParam);
    }

//    @GetMapping(value = "detail")
//    @ApiOperation(value = "师傅详情")
//    public WorkerVO workerDetail(@RequestParam  Long workerId) throws BusinessException{
//        LoginUser loginUser = SecurityContext.getLoginToken();
//        System.out.println(loginUser);
//        WorkerBO workerDetailBO = workerService.getWorkerDetail(workerId);
//        return this.workerAssemble.assembleBO2VO(workerDetailBO);
//    }


    @GetMapping(value = "detail")
    @ApiOperation(value = "师傅详情")
    public WorkerVO workerDetail() throws BusinessException{
        WorkerBO workerDetailBO = workerService.getWorkerDetail();
        return this.workerAssemble.assembleBO2VO(workerDetailBO);
    }

}
