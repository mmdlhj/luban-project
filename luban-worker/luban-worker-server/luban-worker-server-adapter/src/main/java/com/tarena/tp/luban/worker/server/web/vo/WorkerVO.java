package com.tarena.tp.luban.worker.server.web.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class WorkerVO {

    @ApiModelProperty(value = "真实姓名")
    String realName;

    @ApiModelProperty(value = "电话")
    String phone;

    @ApiModelProperty(value = "身份证号")
    String IdCard;

    @ApiModelProperty(value = "区域信息")
    List<AreaVO> areaList;

    @ApiModelProperty(value = "品类信息")
    List<CategoryVO> categoryList;

    @ApiModelProperty(value = "图片列表")
    List<String> attachList;

    @ApiModelProperty(value = "审核状态")
    Integer auditStatus;

    @ApiModelProperty(value = "拒绝原因")
    String rejectReason;

    @ApiModelProperty(value = "状态: 0锁定, 1正常")
    Integer status;

    @ApiModelProperty(value = "账户信息")
    AccountVO accountVO;



}
