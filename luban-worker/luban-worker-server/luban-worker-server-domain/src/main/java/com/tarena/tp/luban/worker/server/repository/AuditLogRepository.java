package com.tarena.tp.luban.worker.server.repository;


import com.tarena.tp.luban.worker.server.bo.AuditBO;

import java.util.List;

public interface AuditLogRepository {

    List<AuditBO> getAudit(Long workerId);
}
