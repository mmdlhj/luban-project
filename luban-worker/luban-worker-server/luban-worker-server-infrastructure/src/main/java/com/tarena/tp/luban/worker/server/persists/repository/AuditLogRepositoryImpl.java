package com.tarena.tp.luban.worker.server.persists.repository;

import com.tarena.tp.luban.worker.po.WorkerAuditLog;
import com.tarena.tp.luban.worker.server.bo.AuditBO;
import com.tarena.tp.luban.worker.server.dao.AuditDAO;
import com.tarena.tp.luban.worker.server.persists.converter.AuditConverter;
import com.tarena.tp.luban.worker.server.repository.AuditLogRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class AuditLogRepositoryImpl implements AuditLogRepository {

    @Resource
    private AuditConverter auditConverter;

    @Resource
    private AuditDAO auditDAO;

    @Override
    public List<AuditBO> getAudit(Long workerId) {
        List<WorkerAuditLog> auditLog = auditDAO.getAuditLog(workerId);
        return auditConverter.pos2bos(auditLog);
    }
}
