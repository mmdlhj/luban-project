package com.tarena.tp.luban.worker.server.dto.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.io.File;
import java.util.List;
import java.util.logging.Level;

@Data
public class IdCardParam {

   @ApiModelProperty("身份证图片地址")
   private String URL;

   @ApiModelProperty("文件id")
   private Integer id;

   @ApiModelProperty("图片类型 type 1:身份证正面 type 2:身份证反面")
   private int type;
}
