package com.tarena.tp.luban.worker.admin.infrastructure.persistence.impl;

import com.tarena.tp.luban.worker.admin.bo.WorkerAreaBO;
import com.tarena.tp.luban.worker.admin.dao.WorkerAreaDAO;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.WorkerAreaConverter;
import com.tarena.tp.luban.worker.admin.repository.WorkerAreaRepository;
import com.tarena.tp.luban.worker.po.WorkerArea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WorkerAdminAreaRepoImpl implements WorkerAreaRepository {
    @Autowired
    private WorkerAreaDAO workerAreaDAO;
    @Autowired
    private WorkerAreaConverter workerAreaConverter;
    @Override
    public List<WorkerAreaBO> getWorkerArea(Long userId) {
        List<WorkerArea> workerAreaByUserId = workerAreaDAO.getWorkerAreaByUserId(userId);

        return workerAreaConverter.poList2BoList(workerAreaByUserId);
    }
}
