package com.tarena.tp.luban.worker.admin.infrastructure.persistence.impl;

import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.dao.WorkerDAO;
import com.tarena.tp.luban.worker.admin.dao.query.WorkerDBPagerQuery;
import com.tarena.tp.luban.worker.admin.infrastructure.persistence.data.converter.WorkerConverter;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.repository.WorkerRepository;
import com.tarena.tp.luban.worker.po.Worker;
import com.tedu.inn.protocol.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WorkerAdminRepoImpl implements WorkerRepository {
    @Autowired
    private WorkerDAO workerDAO;
    //分页查询的dao 入参 DBQuery 包含limit字符串拼接
    @Autowired
    private WorkerConverter workerConverter;
    @Override
    public Long getWorkerCount(WorkerQuery workerQuery) {
        WorkerDBPagerQuery workerDBPagerQuery = workerConverter.toDbPagerQuery(workerQuery);
        //workerDBQuery中多了一个limit 值
        //例如 pageNo=1 pageSize=10 select * from worker limit 0,10
        //pageNo=2 pageSize=10 limit 10,10
        return workerDAO.countWorker(workerDBPagerQuery);
    }
    @Override
    public List<WorkerBO> queryWorkers(WorkerQuery workerBOQuery) {
        WorkerDBPagerQuery workerDBPagerQuery = workerConverter.toDbPagerQuery(workerBOQuery);
        List<Worker> workers = workerDAO.queryWorkers(workerDBPagerQuery);
        return workerConverter.poList2BoList(workers);
    }
    @Override
    public WorkerBO getWorker(Long userId) throws BusinessException {
        Worker worker = workerDAO.getEntity(userId);
        return workerConverter.po2bo(worker);
    }


    @Override
    public Integer disable(Long bankIds) {
        return null;
    }

    @Override
    public Integer enable(Long bankIds) {
        return null;
    }
    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public void updateAuditStatus(WorkerParam workerParam) {
        //1.更新数据库
        Worker worker = workerConverter.param2po(workerParam);
        workerDAO.updateWorkerAuditStatus(worker);
        //2.数据一致性 缓存 cache-aside写流程 删除缓存 redis依赖 yaml 配置类 RedisTemplate
        String key="luban:worker:"+workerParam.getUserId();
        redisTemplate.delete(key);
    }

    @Override
    public void updateCertStatus(WorkerParam workerParam) {
        //1.更新数据库
        Worker worker = workerConverter.param2po(workerParam);
        //update worker set cert_status=1, modified where user_id=#{userId}
        workerDAO.updateWorkerCertStatus(worker);
        //2.数据一致性 缓存 cache-aside写流程 删除缓存 redis依赖 yaml 配置类 RedisTemplate
        String key="luban:worker:"+workerParam.getUserId();
        redisTemplate.delete(key);
    }
}
