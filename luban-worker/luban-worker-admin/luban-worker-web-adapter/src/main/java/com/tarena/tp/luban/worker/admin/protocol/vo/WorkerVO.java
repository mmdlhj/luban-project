/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.admin.protocol.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class WorkerVO {
    @ApiModelProperty("主键 ID")
    private Long id;

    @ApiModelProperty("用户 ID")
    private Long userId;

    @ApiModelProperty("出生日期")
    private Date birthday;

    @ApiModelProperty("区域中文")
    private List<String> areaDetails;

    @ApiModelProperty("服务品类中文")
    private List<String> categoryDetails;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("身份证号")
    private String idCard;

    @ApiModelProperty("账号状态 0:锁定  1:正常")
    private Integer status;

    @ApiModelProperty("审核状态 0:驳回  1:通过")
    private Integer auditStatus;


    @ApiModelProperty("创建人")
    private String createUserName;

    @ApiModelProperty("创建 ID")
    private Long createUserId;

    @ApiModelProperty("更新人ID")
    private Long modifiedUserId;

    @ApiModelProperty("更新人")
    private String modifiedUserName;

    @ApiModelProperty("更新时间")
    private Long gmtModified;

    @ApiModelProperty("创建时间")
    private Long gmtCreate;

    /**
     * 账户信息
     */
    @ApiModelProperty("账户信息")
    private AccountInfoVO accountVO;

    /**
     * 银行卡信息
     */
    @ApiModelProperty("银行卡信息")
    private BankInfoVO bankVO;


}