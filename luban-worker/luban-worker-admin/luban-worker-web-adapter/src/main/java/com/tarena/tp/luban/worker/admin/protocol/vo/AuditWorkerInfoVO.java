package com.tarena.tp.luban.worker.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 审核内容:
 */
@Data
public class AuditWorkerInfoVO {

    @ApiModelProperty("用户 ID")
    private Long workerId;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("身份证号码")
    private String idCard;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("品类详情")
    private List<String> categoryDetails;

    @ApiModelProperty("区域详情")
    private List<String> areaDetails;

    @ApiModelProperty("身份证图片")
    private List<AttachInfoVO> attachInfo;

    @ApiModelProperty("审核状态")
    private Integer  auditStatus;

}
