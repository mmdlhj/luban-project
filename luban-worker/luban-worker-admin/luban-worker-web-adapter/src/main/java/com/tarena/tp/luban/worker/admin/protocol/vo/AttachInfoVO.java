package com.tarena.tp.luban.worker.admin.protocol.vo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.util.List;


/**
 * 基础服务
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AttachInfoVO {

    @ApiModelProperty("图片地址")
    String url;
}
