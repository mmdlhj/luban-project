package com.tarena.tp.luban.worker.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WorkerDetailVO {

    @ApiModelProperty("师傅信息")
    private WorkerInfoVO workerInfoVO;

    @ApiModelProperty("师傅基础信息")
    private BaseWorkerInfoVO baseWorkerInfoVO;

    @ApiModelProperty("认证信息")
    private CertInfoVO certInfoVO;

    @ApiModelProperty("账户信息")
    private AccountInfoVO accountInfoVO;

    @ApiModelProperty("银行卡信息")
    private BankInfoVO bankInfoVO;











}
