package com.tarena.tp.luban.worker.admin.controller;

import com.tarena.tp.luban.worker.admin.assemble.WorkerAssemble;
import com.tarena.tp.luban.worker.admin.assemble.WorkerAuditAssemble;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerAuditQuery;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.protocol.vo.AuditDetailVO;
import com.tarena.tp.luban.worker.admin.protocol.vo.WorkerVO;
import com.tarena.tp.luban.worker.admin.service.WorkerAdminService;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import com.tedu.inn.protocol.model.Result;
import com.tedu.inn.protocol.pager.PagerResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/worker")
public class WorkerAdminController {
    @Autowired
    private WorkerAssemble workerAssemble;
    @Autowired
    private WorkerAdminService workerAdminService;

    @PostMapping("/aduit")
    public Result<PagerResult<WorkerVO>> auditList(@RequestBody WorkerQuery workerQuery){
        //1. 返回 WorkerBO对象
        ListRecordTotalBO<WorkerBO> workerBOS= workerAdminService.auditList(workerQuery);
        //2. 转化
        PagerResult<WorkerVO> result = workerAssemble.assemblePagerResult(workerBOS, workerQuery);
        return new Result(result);
    }
    /**
     * 审核详情查询 ?workerId=1
     * @RequestParam可以控制controller接收 表单参数
     */
    @Autowired
    private WorkerAuditAssemble workerAuditAssemble;
    @PostMapping("/audit/detail")
    public Result<AuditDetailVO> auditDetail(@RequestParam(name="workerId")Long userId) throws BusinessException {
        //1. 查询bo
        WorkerBO workerBO=workerAdminService.auditDetail(userId);
        //2. 转化
        return new Result(workerAuditAssemble.assembleBO2DetailVO(workerBO));
    }
    /**
     * 审核信息提交
     * 通过 驳回
     */
    @PostMapping("/audit/save")
    public Result<Long> auditAdd(@RequestBody AuditParam auditParam) throws BusinessException{
        return new Result(workerAdminService.auditAdd(auditParam));
    }
}
