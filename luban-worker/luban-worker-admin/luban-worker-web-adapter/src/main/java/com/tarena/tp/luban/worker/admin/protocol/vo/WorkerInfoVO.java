package com.tarena.tp.luban.worker.admin.protocol.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 师傅详情---基础信息
 */
@Data
public class WorkerInfoVO {


    @ApiModelProperty("主键 ID")
    private Long id;

    @ApiModelProperty("用户 ID")
    private Long userId;

    @ApiModelProperty("手机号")
    private String phone;

    @ApiModelProperty("真实姓名")
    private String realName;

    @ApiModelProperty("师傅状态 0:无效，1:有效")
    private Integer status;

    /**
     * 需要在数据库中新增区域详情字段
     */
    @ApiModelProperty("区域详情")
    private  String areaDetail;

    /**
     * 需要在数据库中新增品类详情字段
     */
    @ApiModelProperty("品类详情")
    private String categoryDetail;

    /**
     * 账户状态
     */
    @ApiModelProperty("账户状态")
    private String accountStatus;
}
