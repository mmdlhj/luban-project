/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.luban.worker.admin.protocol.query;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tedu.inn.protocol.pager.SimplePagerQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class WorkerQuery extends SimplePagerQuery {
    @ApiModelProperty("师傅编号")
    private String userId;

    @ApiModelProperty("师傅姓名")
    private String realName;

    @ApiModelProperty("师傅手机号")
    private String phone;

    @ApiModelProperty("身份证号码")
    private String idCard;

    @ApiModelProperty("账号状态 0:锁定  1:正常")
    private Integer status;

    @ApiModelProperty("审核状态 0:驳回  1:通过   2:未审核")
    private List<Integer> auditStatus;

    @ApiModelProperty("开始日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Long startDate;

    @ApiModelProperty("结束日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Long endDate;



}