package com.tarena.tp.luban.worker.admin.protocol.param;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 * 审核参数
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuditParam {


    /**
     * 审核意见 审核状态 0:驳回，1:通过  2:未审核
     */
    Integer auditStatus;

    /**
     * 驳回原因
     */
    String rejectReason;

    /**
     * 备注信息
     */
    String remark;

    /**
     * 师傅 ID  Worker_ID 实际值还是userId
     */
    Long workerId;

}
