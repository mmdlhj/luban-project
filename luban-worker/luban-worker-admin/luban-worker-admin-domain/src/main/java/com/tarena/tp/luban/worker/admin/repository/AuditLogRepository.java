package com.tarena.tp.luban.worker.admin.repository;


import com.tarena.tp.luban.worker.admin.bo.AuditBO;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerAreaParam;

import java.util.List;

public interface AuditLogRepository {

    Long save(AuditParam auditParam);

    List<AuditBO> getAudit(Long workerId);
}
