package com.tarena.tp.luban.worker.admin.service;

import com.tarena.tp.attach.server.client.AttachApi;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.query.AttachQuery;
import com.tarena.tp.luban.account.server.client.AccountApi;
import com.tarena.tp.luban.account.server.param.AccountParam;
import com.tarena.tp.luban.worker.admin.bo.AuditBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerAreaBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerBO;
import com.tarena.tp.luban.worker.admin.bo.WorkerCategoryBO;
import com.tarena.tp.luban.worker.admin.protocol.param.AuditParam;
import com.tarena.tp.luban.worker.admin.protocol.param.WorkerParam;
import com.tarena.tp.luban.worker.admin.protocol.query.WorkerQuery;
import com.tarena.tp.luban.worker.admin.repository.AuditLogRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerAreaRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerCategoryRepository;
import com.tarena.tp.luban.worker.admin.repository.WorkerRepository;
import com.tedu.inn.protocol.ListRecordTotalBO;
import com.tedu.inn.protocol.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class WorkerAdminService {
    @Autowired
    private WorkerRepository workerRepository;
    @Autowired(required = false)
    private WorkerAreaRepository workerAreaRepository;
    @Autowired(required = false)
    private WorkerCategoryRepository workerCategoryRepository;
    public ListRecordTotalBO<WorkerBO> auditList(WorkerQuery workerQuery) {
        //目标: 封装一个对象ListRecordTotalBO 2个属性 total总条数 分页list
        //select count(*) from worker where query条件
        //select * from worker where query条件 limit start,rows
        //判断auditStatus的状态值是否是默认空,如果是,填写 0 2
        List<Integer> auditStatus = workerQuery.getAuditStatus();
        if (CollectionUtils.isEmpty(auditStatus)){
            //补充 0 2
            auditStatus.add(0);
            auditStatus.add(2);
        }
        //1. 查询总条数 where audit_status in (0,2)
        Long total = workerRepository.getWorkerCount(workerQuery);
        //2. 判断是否为0 如果为0 说明当前query条件对应的查询结果是空.没有必要查询list
        List<WorkerBO> workerBOS=null;
        if (total>0){
            //不为0的时候 才查询list
            workerBOS = workerRepository.queryWorkers(workerQuery);
            //查询区域信息
            //worker需要补充2个 工作区域 工作种类
            for (WorkerBO workerBO : workerBOS) {
                //缺少的信息 在列表分页查询中不是必须得.
                Long userId = workerBO.getUserId();
                List<String> categoryDetails=getCategoryDetails(userId);
                List<String> areaDetails=getAreaDetails(userId);
                workerBO.setCategoryDetails(categoryDetails);
                workerBO.setAreaDetails(areaDetails);
            }
        }
        //total=0 时候 list不封装
        //将total属性 和list属性 装配到返回值ListRecordTotalBO
        return new ListRecordTotalBO<>(workerBOS,total);
    }

    private List<String> getAreaDetails(Long userId) {
        List<WorkerAreaBO> areas = workerAreaRepository.getWorkerArea(userId);
        List<String> areaDetails=new ArrayList<>();
        if (!CollectionUtils.isEmpty(areas)){
            for (WorkerAreaBO area : areas) {
                areaDetails.add(area.getAreaDetail());
            }
        }
        return areaDetails;
    }

    private List<String> getCategoryDetails(Long userId) {
        List<WorkerCategoryBO> categories = workerCategoryRepository.getWorkerCategory(userId);
        List<String> categoryDetails=new ArrayList<>();
        if (!CollectionUtils.isEmpty(categories)){
            for (WorkerCategoryBO category : categories) {
                categoryDetails.add(category.getCategoryDetail());
            }
        }
        return categoryDetails;
    }
    @Autowired
    private AuditLogRepository auditLogRepository;
    public WorkerBO auditDetail(Long userId) throws BusinessException {
        //1. 使用userId查询workerBO对象 只有非空才继续封装其它数据 区域 种类 审核日志 图片
        WorkerBO worker = workerRepository.getWorker(userId);
        if (worker!=null && worker.getId()!=null){
            //2. 判断非空 挨个根据workerBO属性需要封装其它数据
            //2.1 调用仓储层 查询area详情
            List<String> areaDetails = getAreaDetails(userId);
            worker.setAreaDetails(areaDetails);
            //2.2 调用仓储层 查询category详情
            List<String> categoryDetails = getCategoryDetails(userId);
            worker.setCategoryDetails(categoryDetails);
            //2.3 审核日志 查询仓储层 AuditLogRepository
            List<AuditBO> auditLogs = auditLogRepository.getAudit(userId);
            worker.setWorkerAuditLogs(auditLogs);
            //2.4 资质图片urls
            List<String> urls=getAttachList(100,userId);
            worker.setAttachInfo(urls);
        }
        return worker;
    }
    @Autowired
    private AttachApi attachApi;
    private List<String> getAttachList(int bizType, Long bizId) {
        AttachQuery attachQuery=new AttachQuery();
        attachQuery.setBusinessType(bizType);
        attachQuery.setBusinessId(bizId.intValue());
        List<AttachDTO> attachDTOS = attachApi.getAttachInfoByParam(attachQuery);
        List<String> attachUrls=new ArrayList<>();
        //查询结果非空,封装返回数据
        if(!CollectionUtils.isEmpty(attachDTOS)){
            //dto有一个图片fileUuid 拼接urlPrefix
            String urlPrefix="http://localhost:8092/static/";
            for (AttachDTO attachDTO : attachDTOS) {
                String fileUuid=attachDTO.getFileUuid();
                String url=urlPrefix+fileUuid;
                attachUrls.add(url);
            }
        }
        return attachUrls;
    }
    @Autowired
    private AccountApi accountApi;
    // 审核流转状态是错误的. audit_status=2-0 2-1 0-1  1-0(不合理)
    public Long auditAdd(AuditParam auditParam) throws BusinessException {
        //1.流转状态 判断 不合理流转状态
        WorkerBO worker = workerRepository.getWorker(auditParam.getWorkerId());
        Integer haveAuditStatus=worker.getAuditStatus();
        Integer addAuditStatus=auditParam.getAuditStatus();
        checkStatusTransfer(haveAuditStatus,addAuditStatus);//检查流转状态
        //2.写入日志
        Long auditLogId=auditLogRepository.save(auditParam);
        //3.修改师傅状态值
        WorkerParam workerParam=new WorkerParam();
        workerParam.setAuditStatus(addAuditStatus);
        workerParam.setUserId(auditParam.getWorkerId());
        //update worker set audit_status=#{1|0} set modified_user_id modified_user_name gmt_modified
        //where user_id=#{userId}
        workerRepository.updateAuditStatus(workerParam);
        //4.创建账户 条件(审核通过,userId从来没有安定过账户);
        if (worker.getCertStatus()==0 && addAuditStatus==1 ){
            AccountParam accountParam=new AccountParam();
            accountParam.setUserId(auditParam.getWorkerId());
            accountParam.setUserName(worker.getRealName());
            accountParam.setUserPhone(worker.getPhone());
            Long result = accountApi.create(accountParam);
            //如果返回值是1,说明创建成功,将师傅的认证状态修改为1
            if (result==1){
                WorkerParam workerParam1=new WorkerParam();
                workerParam1.setCertStatus(1);
                workerParam1.setUserId(worker.getUserId());
                workerRepository.updateCertStatus(workerParam1);
            }
            log.info("当前审核提交 暂时无法创建账户");
        }
        return auditLogId;
    }

    private void checkStatusTransfer(Integer haveAuditStatus,Integer addAuditStatus) throws BusinessException{
        //TODO 0-2 1-2 2-2 1-0 0-1 如果流转状态是这里定义的非法流转中任意一种,抛异常
    }
}
